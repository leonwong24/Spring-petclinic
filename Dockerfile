FROM openjdk:17
ADD target/spring-petclinic-3.0.0-SNAPSHOT.jar spring-petclinic-3.0.0-SNAPSHOT.jar
EXPOSE 80
ENTRYPOINT ["sh", "-c", "java -Dserver.port=$PORT -jar spring-petclinic-3.0.0-SNAPSHOT.jar"]
